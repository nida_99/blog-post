# Strict vs Loose Equality and Type coercion in JavaScript
In javascript, there are three different value-comparison operations. Strict equality comparison, Abstract equality comparison and object.is. In this blogpost, we will go through Strict equality comparison and Abstract equality comparison.

### Strict equality :
It compares two values for equality. If values have different data types, it will return false. There is no implicit conversion before they are compared. It uses Strict Equality Comparison Algorithm to compare two operands:
* If operands are of different types, return false.
```js
console.log("1" === 1);                    //false
console.log(false === 0);                  //false
console.log(true === 1);                   //false
```
* If both operands are objects, return true if they are the same object.
```js
const obj1 = { batch: "JavaScript" };
const obj2 = { batch: "JavaScript" };
console.log(obj1 === obj2);                //false
console.log(obj1 === obj1);                //true
```
* If both operands are either null or undefined, return true.
```js
console.log(null === null);               //true
console.log(undefined === undefined);     //true
console.log(null === undefined);          //false
```
* It treats NaN as unequal to every other value including itself. 
```js
console.log(NaN === NaN);                 //false
```
* +0 and -0 are considered to be the same value.
```js
console.log(0 === -0);                    //true
```
* Booleans must be both true or false.
```js
console.log(true === true);               //true
console.log(false === false);             //true
console.log(false === true);              //false
```

### Loose equality :
It converts the operand to the same data types and then checks for values. **The process of implicit conversion from one data type to another is known as Type coercion**. After conversion, the final equality comparison is performed exactly as strict equality performs it. Loose equality uses abstract equality algorithms to decide how to compare values.
It will check if the data type is the same. If same, then it will check values else it will follow different steps :
* If one operand is Null and the other is undefined, it will return true.

```js
console.log(null == undefined);            //true
console.log(null == null);                 //true
console.log(undefined == undefined);       //true
```
* If we compare string with number, it will coerce the string value to number value and then check.
```js
console.log("5" == 5);                     //true
console.log("1" + "3" == 13);              //true
```
* If we compare boolean with number , then it will coerce boolean to number and check.
```js
console.log(false == 0);                   //true
console.log(true == 1);                    //true
```                 
* If we compare an object with a number, string or a symbol, it will coerce the object to a primitive.
```js
console.log([1, 2] == '1,2');               //true
```
* If either operand is NaN, it will return false.
```js
console.log(NaN == NaN);                  //false
```

### Type coercion :
Type Coercion refers to the automatic or implicit conversion of values from one type to another. When we try to perform operations in JavaScript using different value types, JavaScript coerces the value types for us implicitly.

* To String Coercion:
When we try to add two values and one of them is string, JavaScript will try to automatically convert the other values to a string so that they can be appended.
```js
console.log(10 + '10');                        //1010
console.log('10' + 10);                        //1010
```

* To Number Coercion:
When we perform mathematical operations except addition like Subtraction(-), Multiplication(*), Division(/), and Modulus(%) with string then output of the expression is converted into a number implicitly.
```js
console.log("10" - 10);                          // 0
console.log(10 - '10');                          // 0
console.log("6" / 2);                            // 3
console.log("7" % 3);                            // 1
console.log(5 - "");                             // 5
```
* To Boolean Coercion:
In boolean coercion, the boolean values such as true and false are converted to a number.
```js
console.log(true == 1);                           //true
console.log(false == 0);                          //true
```

